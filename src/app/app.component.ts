import { Component,OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {CustomValidators} from './custom-validators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	form:FormGroup;
	private statuses = [
		{
			value: 'stable',
			display: 'Stable',
		},
		{
			value: 'critical',
			display: 'Critical',
		},
		{
			value: 'finished',
			display: 'Finished',
		},
	];
	forbiddenProjectNames = ['test'];

	ngOnInit() {
		this.form = new FormGroup({
			// 'name': new FormControl(null, [Validators.required,this.validateProjectName.bind(this)]),
			'name': new FormControl(null, [Validators.required,CustomValidators.validateProjectName],CustomValidators.asyncValidateProjectName),
			'email': new FormControl(null, [Validators.required,Validators.email],this.validateEmail),
			'dropdown': new FormControl(null, [Validators.required]),
		});
		this.form.valueChanges.subscribe((status)=>console.log(status) );
	}

	/*
	 * Perfectly fine to put this in this file
	 * but Max makes his own validators class. Might be good idea.
	 * let's give it a try for documentation purposes
	 */

	// validateProjectName(control:FormControl): { [s:string]:boolean } {
	// 	var value = control.value?control.value.toLowerCase():control.value;
	// 	if ( this.forbiddenProjectNames.indexOf(value) !== -1  ) {
	// 		return {'forbiddenProjectName':true};
	// 	}		
	// 	return null;
	// }

	validateEmail(control:FormControl): Promise<any> | Observable<any> {
		const promise = new Promise<any>( (resolve,reject)=>{
			setTimeout(()=>{
				if (control.value === 'javier.s.fraga@gmail.com') {
					resolve({'forbiddenEmail':true})
				} else {
					resolve(null)
				}
			},2000);
		});
		return promise;
	}

	outputForm() {
		console.log(this.form)
	}

}
