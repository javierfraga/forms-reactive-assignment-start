import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

export class CustomValidators {
	private forbiddenProjectNames = ['test'];
	/*
	 * I can only get this working on static functions
	 * I also cannot get the forbiddenProjectNames working in static function has to be non-static
	 * but if I do that then function not recognized outside in other class when imports
	 */
	static validateProjectName(control: FormControl): {[s:string]:boolean} {
		var value = control.value?control.value.toLowerCase():control.value;
		// if (this.forbiddenProjectNames.indexOf(value) !== -1) {
		if (value === 'test') {
			return {'forbiddenProjectName':true};
		} else {
			return null;
		}
	}
	static asyncValidateProjectName(control: FormControl): Promise<any> | Observable<any> {
		const promise = new Promise<any>( (resolve,reject)=>{
			setTimeout(()=>{
				if (control.value === 'javier') {
					resolve({'forbiddenProjectName':true})
				} else {
					resolve(null)
				}
			},2000);
		});
		return promise;
	}
}